package pro.extenza.quickpoint;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class YaPhotoActivity extends Activity {
	String TAG = "QUICKPOINT";
	String yaToken;
	String yaHost = "api-fotki.yandex.ru";
	SharedPreferences prefs;
	String serviceDocument;
	final String SVCDOCFILENAME = "svcdoc";
	final String ALBUMLISTFILENAME = "albumlist";
	String albumListLink = "";
	String albumListTitle = "";
	ArrayList<String[]> albumArray;
	YaServiceProgressDialog yaSPDialog;
	File imageToUpload;
	final String UPLOADEDIMGINFO = "imginfo";
	String uploadedImageLink = "";
	ProgressDialog progressDiag;
	final int DIALOG_PROBLEM = 1;
	final int NETWORK_PROBLEM = 2;
	final int SERVER_RESPONSE_PROBLEM = 3;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ya_photo);
		prefs = getApplicationContext().getSharedPreferences("prefs", 0);
		if (!prefs.contains("yaToken")) {
			yaAuthorize();
		}
		yaToken = prefs.getString("yaToken", "");
		// now let's call YaServiceProgressDialog and retrieve service document
		if (!yaToken.equals(""))
			new GetServiceDocumentTask().execute();

	}

	protected Dialog onCreateDialog(int id) {
		
			AlertDialog.Builder adb = new AlertDialog.Builder(this);
			// ���������
			adb.setTitle("ERROR");
			// ���������
			switch(id){
			case 1:
				adb.setMessage("Unexpected error. Please report to @torgash."); break;
			case 2:
				adb.setMessage("Network problem"); break;
			case 3:
				adb.setMessage("Server response problem"); break;
			}
			
			// ������
			adb.setIcon(android.R.drawable.ic_dialog_alert);
			// ������ �������������� ������
			adb.setPositiveButton("Close", myClickListener);

			return adb.create();
		
		
	}

	OnClickListener myClickListener = new OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
			switch (which) {
			// ������������� ������
			case Dialog.BUTTON_POSITIVE:
				finish();

				break;

			}
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.ya_photo, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	// yaAuthorize() is void because login dialog returns value through
	// getTokenUrl()
	public void yaAuthorize() {

		DialogFragment yaLoginDig;
		yaLoginDig = new YaLoginDialog();
		yaLoginDig.setCancelable(false);
		yaLoginDig.show(getFragmentManager(), "yalogindig");

	}

	public void getTokenUrl(String returnedUrl) {
		String tokenString = "";
		if (!returnedUrl.contains("error")) {
			tokenString = returnedUrl.substring(
					returnedUrl.indexOf("access_token=") + 13,
					returnedUrl.indexOf("&token_type"));
		}// insert "else" and handle unauthorized errors;
		Log.d("QUICKPOINT", "The URL was " + returnedUrl);
		Log.d("QUICKPOINT", "Got a token " + tokenString + " from it.");
		Editor editor = prefs.edit();
		Log.d(TAG, "token put to preferences");
		editor.putString("yaToken", tokenString);
		editor.apply();
		new GetServiceDocumentTask().execute();
	}

	public class GetServiceDocumentTask extends AsyncTask<Void, String, String> {
		// here we retriever service document and album list

		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub

			// first let's get user data from /api/me because the app is unaware
			// of username during OAuth authorization
			publishProgress("Getting Service document...");
			HttpClient httpclient = new DefaultHttpClient();
			HttpGet httpget = new HttpGet("http://" + yaHost + "/api/me/");
			try {
				// Add your data
				// httpget.setHeader("Host", yaHost);
				httpget.setHeader("Authorization", "OAuth " + prefs.getString("yaToken", ""));
				Log.d(TAG, "GET " + httpget.getURI());
				Log.d(TAG, "Request line: " + httpget.getRequestLine());
				Header[] getHeaders = httpget.getAllHeaders();
				for (int i = 0; i < getHeaders.length; i++) {
					Log.d(TAG, "Header " + i + ": " + getHeaders[i].getName()
							+ ": " + getHeaders[i].getValue());
				}

				// Execute HTTP Post Request
				HttpResponse response = httpclient.execute(httpget);
				Log.d(TAG, "api/me result is: "
						+ +response.getStatusLine().getStatusCode()
						+ response.getStatusLine().getReasonPhrase());
				Log.d(TAG, "Full api/me response is " + response.toString());
				Header[] headers = response.getAllHeaders();
				Log.d(TAG, "Excluded " + headers.length + " headers:");
				for (int i = 0; i < headers.length; i++) {
					Log.d(TAG, "Header " + i + ": " + headers[i].getName()
							+ ": " + headers[i].getValue());
				}
				if (response.getStatusLine().getStatusCode() == java.net.HttpURLConnection.HTTP_OK) {
					HttpEntity httpEntity = response.getEntity();
					Log.d(TAG, "Full entity is: " + httpEntity.toString());
					InputStream userDataStream = AndroidHttpClient
							.getUngzippedContent(httpEntity);
					Log.d(TAG,
							"Full userdatastream is: "
									+ userDataStream.toString());
					BufferedReader bufferedReader = new BufferedReader(
							new InputStreamReader(userDataStream));
					StringBuilder responseBuilder = new StringBuilder();
					char[] buff = new char[1024 * 512];
					int read;
					while ((read = bufferedReader.read(buff)) != -1) {
						responseBuilder.append(buff, 0, read);
						Log.d("DOWNLOAD", "������� " + responseBuilder.length());
					}
					String result = responseBuilder.toString();
					Log.d(TAG, "Response from /api/me is: " + result);
					publishProgress("done");
					return result;
				} else
					return null;
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				Log.d(TAG, "ClientProtocolException raised: " + e);
				showDialog(NETWORK_PROBLEM);
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				Log.d(TAG, "IOException raised: " + e);
				showDialog(NETWORK_PROBLEM);
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			yaSPDialog = new YaServiceProgressDialog();
			yaSPDialog.show(getFragmentManager(), "yaSPdig");

		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			File svcdocfile = new File(getFilesDir(), SVCDOCFILENAME);
			Log.d(TAG, "File created: " + svcdocfile.toString());
			try {
				// �������� ����� ��� ������

				BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
						openFileOutput(SVCDOCFILENAME, MODE_PRIVATE)));
				// ����� ������
				if (result != null) {
					bw.write(result);
				}
				// ��������� �����
				bw.close();

				Log.d(TAG, "���� �������: " + svcdocfile.getAbsolutePath()
						+ " ");
				InputStream in = null;
				try {
					in = new BufferedInputStream(
							new FileInputStream(svcdocfile));
					Log.d(TAG, "File reading: " + in.toString());
				} finally {
					if (in != null)
						in.close();
				}

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			// yaSPDialog.dismiss();
			yaSPDialog.onProcessChanged("Parsing service doc...");
			// here and therefore we try to parse XMl;
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			Document dom = null;
			try {

				// Using factory get an instance of document builder
				DocumentBuilder db = dbf.newDocumentBuilder();

				// parse using builder to get DOM representation of the XML file
				dom = db.parse(svcdocfile);
				Log.d(TAG, dom.toString());

			} catch (ParserConfigurationException pce) {
				pce.printStackTrace();
				showDialog(SERVER_RESPONSE_PROBLEM);
				Log.d(TAG, "ParserConfigurationException: " + pce.toString());
			} catch (SAXException se) {
				se.printStackTrace();
				showDialog(SERVER_RESPONSE_PROBLEM);
				Log.d(TAG, "SAX Exception: " + se.toString());
			} catch (IOException ioe) {
				ioe.printStackTrace();
				showDialog(SERVER_RESPONSE_PROBLEM);
				Log.d(TAG, "IOException: " + ioe.toString());
			}

			// let's assume we've read the file with servicedoc. Let's look
			// what's inside.
			Element docEle = null;
			try {
				docEle = dom.getDocumentElement();
			} catch (Exception e) {
				Log.d(TAG, "Unknown exception: " + e.toString());
				e.printStackTrace();
				AlertDialog alert = new AlertDialog(YaPhotoActivity.this) {

				};
			}
			// get a nodelist of elements
			NodeList collection = null;
			if (docEle != null) {
				collection = docEle.getElementsByTagName("app:collection");
			}
			if (collection != null && collection.getLength() > 0) {
				Log.d(TAG, "Found collections: " + collection.getLength());
				for (int i = 0; i < collection.getLength(); i++) {
					Element collectionEl = (Element) collection.item(i);
					Log.d(TAG,
							"Trying Attribute: "
									+ collectionEl.getAttribute("href"));

					if (collectionEl.getAttribute("id").equals("album-list")) {
						albumListLink = collectionEl.getAttribute("href");
						NodeList collectionTitles = collectionEl
								.getElementsByTagName("atom:title");
						Log.d(TAG, "Found atom:title nodes: "
								+ collectionTitles.getLength());
						Log.d(TAG, "Retrieving atom:title node: "
								+ collectionTitles.item(0).getTextContent());
						retrieveAlbumList(yaSPDialog, albumListLink,
								albumListTitle);
					}

				}
			}
		}

		@Override
		protected void onProgressUpdate(String... processName) {
			// TODO Auto-generated method stub
			yaSPDialog.onProcessChanged(processName[0]);
		}

		@Override
		protected void onCancelled() {
			// TODO Auto-generated method stub
			super.onCancelled();
		}

	}

	public void retrieveAlbumList(YaServiceProgressDialog dialog,
			String listLink, String listTitle) {
		dialog.onProcessChanged("Getting album list...");
		// now that we have the link to album list, let's retrieve it.
		new GetAlbumListTask().execute();
	}

	public class GetAlbumListTask extends AsyncTask<Void, String, String> {
		// here we retriever service document and album list

		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub

			// first let's get user data from /api/me because the app is unaware
			// of username during OAuth authorization

			HttpClient httpclient = new DefaultHttpClient();
			HttpGet httpget = new HttpGet(albumListLink + "published/");
			try {
				// Add your data
				// httpget.setHeader("Host", yaHost);
				httpget.setHeader("Authorization", "OAuth " + prefs.getString("yaToken", ""));
				Log.d(TAG, "GET " + httpget.getURI());
				Log.d(TAG, "Request line: " + httpget.getRequestLine());
				Header[] getHeaders = httpget.getAllHeaders();
				for (int i = 0; i < getHeaders.length; i++) {
					Log.d(TAG, "Header " + i + ": " + getHeaders[i].getName()
							+ ": " + getHeaders[i].getValue());
				}

				// Execute HTTP Post Request
				HttpResponse response = httpclient.execute(httpget);
				Log.d(TAG, "/albums/ result is: "
						+ +response.getStatusLine().getStatusCode()
						+ response.getStatusLine().getReasonPhrase());
				Log.d(TAG, "Full /albums/ response is " + response.toString());
				Header[] headers = response.getAllHeaders();
				Log.d(TAG, "Excluded " + headers.length + " headers:");
				for (int i = 0; i < headers.length; i++) {
					Log.d(TAG, "Header " + i + ": " + headers[i].getName()
							+ ": " + headers[i].getValue());
				}
				if (response.getStatusLine().getStatusCode() == java.net.HttpURLConnection.HTTP_OK) {
					HttpEntity httpEntity = response.getEntity();
					Log.d(TAG, "Full entity is: " + httpEntity.toString());
					InputStream userDataStream = AndroidHttpClient
							.getUngzippedContent(httpEntity);
					Log.d(TAG,
							"Full userdatastream is: "
									+ userDataStream.toString());
					BufferedReader bufferedReader = new BufferedReader(
							new InputStreamReader(userDataStream));
					StringBuilder responseBuilder = new StringBuilder();
					char[] buff = new char[1024 * 512];
					int read;
					while ((read = bufferedReader.read(buff)) != -1) {
						responseBuilder.append(buff, 0, read);
						Log.d(TAG, "������� " + responseBuilder.length());
					}
					String result = responseBuilder.toString();
					Log.d(TAG, "Response from /api/me is: " + result);

					return result;
				} else
					return null;
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				Log.d(TAG, "ClientProtocolException raised: " + e);
				showDialog(NETWORK_PROBLEM);
				e.printStackTrace();
				return null;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				Log.d(TAG, "IOException raised: " + e);
				showDialog(NETWORK_PROBLEM);
				e.printStackTrace();
				return null;
			}
			
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub

		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			File albumlistfile = new File(getFilesDir(), ALBUMLISTFILENAME);
			Log.d(TAG, "File created: " + albumlistfile.toString());
			try {
				// �������� ����� ��� ������

				BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
						openFileOutput(ALBUMLISTFILENAME, MODE_PRIVATE)));
				// ����� ������
				if (result != null) {
					bw.write(result);
				}
				// ��������� �����
				bw.close();

				Log.d(TAG, "���� �������: " + albumlistfile.getAbsolutePath()
						+ " ");
				InputStream in = null;
				try {
					in = new BufferedInputStream(new FileInputStream(
							albumlistfile));
					Log.d(TAG, "File reading: " + in.toString());
				} finally {
					if (in != null)
						in.close();
				}

			} catch (FileNotFoundException e) {
				e.printStackTrace();
				Log.d(TAG, "FileNotFound Exception: "+e.toString());
				showDialog(DIALOG_PROBLEM);
			} catch (IOException e) {
				e.printStackTrace();
				Log.d(TAG, "FileNotFound Exception: "+e.toString());
				showDialog(DIALOG_PROBLEM);
			}

			// yaSPDialog.dismiss();

			// here and therefore we try to parse XMl;
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			Document dom = null;
			try {

				// Using factory get an instance of document builder
				DocumentBuilder db = dbf.newDocumentBuilder();

				// parse using builder to get DOM representation of the XML file
				dom = db.parse(albumlistfile);
				Log.d(TAG, dom.toString());

			} catch (ParserConfigurationException pce) {
				pce.printStackTrace();
				showDialog(SERVER_RESPONSE_PROBLEM);
			} catch (SAXException se) {
				se.printStackTrace();
				showDialog(SERVER_RESPONSE_PROBLEM);
			} catch (IOException ioe) {
				ioe.printStackTrace();
				showDialog(SERVER_RESPONSE_PROBLEM);
			}

			// let's assume we've read the file with servicedoc. Let's look
			// what's inside.

			try{
			Element docEle = dom.getDocumentElement();
			
			// get a nodelist of elements
			NodeList collection = docEle.getElementsByTagName("entry");
			albumArray = new ArrayList<String[]>();

			if (collection != null && collection.getLength() > 0) {
				Log.d(TAG, "Found album entries: " + collection.getLength());
				for (int i = 0; i < collection.getLength(); i++) {
					Element collectionEl = (Element) collection.item(i);
					NodeList links = collectionEl.getElementsByTagName("link");
					Log.d(TAG, "Found link entries in album " + i + ": "
							+ links.getLength());
					for (int j = 0; j < links.getLength(); j++) {
						Element link = (Element) links.item(j);
						if (link.getAttribute("rel").equals("photos")) {
							String albumName = collectionEl
									.getElementsByTagName("title").item(0)
									.getTextContent();
							String albumURN = collectionEl
									.getElementsByTagName("id").item(0)
									.getTextContent();
							NodeList imageCount = collectionEl
									.getElementsByTagName("f:image-count");
							String albumImageCount = ((Element) imageCount
									.item(0)).getAttribute("value");
							String albumLink = link.getAttribute("href");
							Log.d(TAG, "putting album " + albumName
									+ "to ArrayLists");
							albumArray.add(new String[] { albumName, albumLink,
									albumImageCount, albumURN });
							populateAlbumList(albumArray);
						}

					}

				}
			}
			}catch(Exception e){
				Log.d(TAG, "Exception: " + e.toString());
				e.printStackTrace();
				showDialog(DIALOG_PROBLEM);
			}
		}

		@Override
		protected void onProgressUpdate(String... processName) {
			// TODO Auto-generated method stub

		}

		@Override
		protected void onCancelled() {
			// TODO Auto-generated method stub
			super.onCancelled();
		}

	}

	public void populateAlbumList(ArrayList<String[]> albumList) {
		yaSPDialog.dismiss();
		ListView lvAlbumList = (ListView) findViewById(R.id.lvAlbumList);
		ArrayList<Map<String, Object>> data = new ArrayList<Map<String, Object>>(
				albumList.size());
		Map<String, Object> m;
		for (String[] x : albumList) {
			m = new HashMap<String, Object>();
			m.put("albumName", x[0]);
			m.put("imageCount", "photos: " + x[2]);
			data.add(m);
		}
		String[] from = { "albumName", "imageCount" };

		int[] to = { R.id.tvAlbumName, R.id.tvImgCount };

		SimpleAdapter sAdapter = new SimpleAdapter(this, data,
				R.layout.mymessageslistelement, from, to);

		lvAlbumList.setAdapter(sAdapter);
		lvAlbumList.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				String[] albname = albumArray.get(position);
				String link = albname[1];
				Log.d(TAG, "itemClick: position = " + position + ", id = "
						+ albname[0]);
				startImageUploading(link);
			}
		});

	}

	public void startImageUploading(String _link) {
		File image = getIntentExtra();
		Log.d(TAG, "File to upload is acquired" + image.toString());

		new PostImageUploadTask().execute(_link, image.getAbsolutePath());

	}

	public File getIntentExtra() {
		Intent intent = getIntent();
		String action = intent.getAction();
		String type = intent.getType();
		String action_required = Intent.ACTION_SEND;
		String type_required = "image";
		Log.d(TAG, "we come here to pick the image");
		if (Intent.ACTION_SEND.equals(action) && type != null) {
			if (type.startsWith(type_required)) {
				Uri imageUri = (Uri) intent
						.getParcelableExtra(Intent.EXTRA_STREAM);
				Log.d(TAG, "Image URI acquired: " + imageUri.toString());
				if (imageUri != null) {
					// Update UI to reflect image being shared
					Cursor cursor = getContentResolver()
							.query(imageUri,
									new String[] { android.provider.MediaStore.Images.ImageColumns.DATA },
									null, null, null);
					Log.d(TAG, "Cursor retrieved, " + cursor.getColumnCount()
							+ " columns, " + cursor.toString());
					cursor.moveToFirst();
					for (int i = 0; i < cursor.getColumnCount(); i++) {
						String columnName = cursor.getColumnName(i);
						Log.d(TAG, "Cursor column #" + i + ": " + columnName
								+ cursor.getString(i));
					}
					String imageFilePath = cursor.getString(0);
					Log.d(TAG, "retrieving imageFilePath from URI, got: "
							+ imageFilePath);
					File image = new File(imageFilePath.toString());
					Log.d(TAG, "File to upload: " + image.getAbsolutePath());
					return image;
				}
			}
		}
		return null;
	}

	public class PostImageUploadTask extends AsyncTask<String, String, String> {
		// here we retriever service document and album list

		@SuppressWarnings("deprecation")
		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			String link = params[0];
			File image = new File(params[1]);
			HttpClient httpClient = new DefaultHttpClient();
			httpClient.getParams().setParameter(
					CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
			String albumLink = link;
			HttpPost httpPost = new HttpPost(albumLink);
			httpPost.setHeader("Authorization", "OAuth " + prefs.getString("yaToken", ""));
			Log.d(TAG, "Creating HTTP POST for link: " + albumLink);
			Log.d(TAG, "Testing file existance: " + image.exists() + ", "
					+ image.getAbsolutePath());
			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
			builder.addTextBody("access", "public");
			Bitmap bitmap = BitmapFactory.decodeFile(params[1]);
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream);
			byte[] byte_arr = stream.toByteArray();
			ByteArrayBody fileBody = new ByteArrayBody(byte_arr,
					image.getName());
			builder.addPart("image", fileBody);

			httpPost.setEntity(builder.build());
			Log.d(TAG, "Processing the request: " + httpPost.getRequestLine());

			try {
				HttpResponse response;
				response = httpClient.execute(httpPost);
				HttpEntity resEntity = response.getEntity();
				Log.d(TAG, "Response: " + response.getStatusLine());

				InputStream userDataStream = AndroidHttpClient
						.getUngzippedContent(resEntity);
				Log.d(TAG,
						"Full userdatastream is: " + userDataStream.toString());
				BufferedReader bufferedReader = new BufferedReader(
						new InputStreamReader(userDataStream));
				StringBuilder responseBuilder = new StringBuilder();
				char[] buff = new char[1024 * 512];
				int read;
				while ((read = bufferedReader.read(buff)) != -1) {
					responseBuilder.append(buff, 0, read);
					Log.d(TAG, "������� " + responseBuilder.length());
				}
				String result = responseBuilder.toString();
				Log.d(TAG, "POST response is: " + result);
				return result;
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Log.d(TAG, "ClientProtocolException: " + e.toString());
				showDialog(NETWORK_PROBLEM);
				return null;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				Log.d(TAG, "IOException: " + e.toString());
				e.printStackTrace();
				showDialog(NETWORK_PROBLEM);
				return null;
			}

		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			progressDiag = new ProgressDialog(YaPhotoActivity.this);
			progressDiag.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);

			ProgressDialog.show(YaPhotoActivity.this, "Please wait...",
					"Uploading in progress", true, false);

		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			if (result != null) {
				progressDiag.dismiss();
				File postResult = new File(getFilesDir(), UPLOADEDIMGINFO);
				Log.d(TAG, "File created: " + postResult.toString());
				try {
					// �������� ����� ��� ������

					BufferedWriter bw = new BufferedWriter(
							new OutputStreamWriter(openFileOutput(
									UPLOADEDIMGINFO, MODE_PRIVATE)));
					// ����� ������
					if (result != null) {
						bw.write(result);
					}
					// ��������� �����
					bw.close();

					Log.d(TAG,
							"��������������� �������: "
									+ postResult.getAbsolutePath() + " ");
					InputStream in = null;
					try {
						in = new BufferedInputStream(new FileInputStream(
								postResult));
						Log.d(TAG, "File reading: " + in.toString());
					} finally {
						if (in != null)
							in.close();
					}

				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				// yaSPDialog.dismiss();

				// here and therefore we try to parse XMl;
				DocumentBuilderFactory dbf = DocumentBuilderFactory
						.newInstance();
				Document dom = null;
				try {

					// Using factory get an instance of document builder
					DocumentBuilder db = dbf.newDocumentBuilder();

					// parse using builder to get DOM representation of the XML
					// file
					dom = db.parse(postResult);
					Log.d(TAG, dom.toString());

				} catch (ParserConfigurationException pce) {
					pce.printStackTrace();
				} catch (SAXException se) {
					se.printStackTrace();
				} catch (IOException ioe) {
					ioe.printStackTrace();
				}

				// let's assume we've read the file with servicedoc. Let's look
				// what's inside.

				Element docEle = dom.getDocumentElement();

				// get a nodelist of elements
				NodeList collection = docEle.getElementsByTagName("content");
				if (collection != null && collection.getLength() > 0) {
					Log.d(TAG, "Found content links: " + collection.getLength());

					Element collectionEl = (Element) collection.item(0);
					Log.d(TAG,
							"Trying Attribute: "
									+ collectionEl.getAttribute("src"));

					uploadedImageLink = collectionEl.getAttribute("src");
					goToPointActivity(uploadedImageLink);
				}
			}

		}

		@Override
		protected void onProgressUpdate(String... processName) {
			// TODO Auto-generated method stub

		}

		@Override
		protected void onCancelled() {
			// TODO Auto-generated method stub
			super.onCancelled();
		}

	}

	public void goToPointActivity(String link) {
		Intent intent = new Intent(this, MainActivity.class);
		intent.putExtra("link", link);
		startActivity(intent);
		finish();
	}
}
