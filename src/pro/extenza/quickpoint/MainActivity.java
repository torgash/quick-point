package pro.extenza.quickpoint;

import java.net.URI;
import java.net.URL;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity{
	public static final String USER_LOGIN = "USER_LOGIN";
	public static final String USER_PASSWORD = "USER_PASSWORD";
	public static final String USER_TOKEN = "USER_TOKEN";
	public static final String USER_CSRF = "USER_CSRF";
	SharedPreferences prefs;
	Context contex;
	Button btnSend;
	Button btnCancel;	
	EditText etPostText;
	EditText etPostTags;
	Button btnLogin;
	EditText etLogin;
	EditText etPassword;
	public static String message_id;
	int authResult;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		
		contex  = getApplicationContext();
		prefs = contex.getSharedPreferences("prefs", MODE_PRIVATE);
		Log.d("DOWNLOAD", prefs.getString("csrf_token", ""));
		if(prefs.contains("csrf_token")){
		setContentView(R.layout.main);
		btnSend = (Button)findViewById(R.id.btnSend);
		btnCancel = (Button)findViewById(R.id.btnCancel);
		etPostText = (EditText)findViewById(R.id.etPostText);
		etPostTags = (EditText)findViewById(R.id.etPostTags);
		getIntentExtra();
		}else {
			setContentView(R.layout.login);
			btnLogin = (Button)findViewById(R.id.btnLogin);
			etLogin = (EditText)findViewById(R.id.etLogin);
			etPassword = (EditText)findViewById(R.id.etPassword);
		}
		
	}
	protected Dialog onCreateDialog(int id) {
		
		OnClickListener myClickListener = new OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
				// ������������� ������
				case Dialog.BUTTON_POSITIVE:
					finish();

					break;
				case Dialog.BUTTON_NEGATIVE:
					Intent webIntent = new Intent();
					webIntent.setAction(Intent.ACTION_VIEW);
					String messageURL = "http://point.im/" + message_id;
					
					webIntent.setData(Uri.parse(messageURL));
					startActivity(webIntent);
					finish();
				}
			}
		};
		AlertDialog.Builder adb = new AlertDialog.Builder(this);
		// ���������
		adb.setTitle("POSTED");
		// ���������
		switch(id){
		case 1:
			adb.setMessage("Your message: #" + message_id); break;
		
		}
		
		// ������
		adb.setIcon(android.R.drawable.ic_dialog_alert);
		// ������ �������������� ������
		adb.setPositiveButton("OK", myClickListener);
		adb.setNegativeButton("WEB", myClickListener);
		return adb.create();
	
	
}


	public void onClickLogin(View v){
		final PointHttpPost authorize = new PointHttpPost(contex, "/login", etLogin.getText().toString(), etPassword.getText().toString());
 	   Log.d("buttons", "button was pressed");
 	 
 	   Thread t = new Thread(new Runnable(){
			   
	    	public void run(){
	    		authResult = 0;
	    		authResult = authorize.getTokens();
	    		return;
	    	}
	    });
 	   t.start();
  	    try {
			t.join();
		
  	    switch(authResult){
		case 0:
			setContentView(R.layout.main);
			btnSend = (Button)findViewById(R.id.btnSend);
			btnCancel = (Button)findViewById(R.id.btnCancel);
			etPostText = (EditText)findViewById(R.id.etPostText);
			etPostTags = (EditText)findViewById(R.id.etPostTags);
			getIntentExtra();
			break;
		case 1: 
			etLogin.setText("");
			etPassword.setText("");
			etLogin.requestFocus();
			Toast.makeText(contex, "Not authorized.\nWrong password?", Toast.LENGTH_LONG).show();
		
		break;
		case 2:
			Toast.makeText(contex, "Something wrong with connection", Toast.LENGTH_LONG).show();
			break;
		case 3: 
			Toast.makeText(contex, "IO error, try reloading the application", Toast.LENGTH_LONG).show();
			break;
		}
  	 } catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
  	    	
	   }
  
	int postResult;
	public void onClickSend(View v){
		final PointHttpPost newPost = new PointHttpPost(contex, "/post", etPostText.getText().toString(), etPostTags.getText().toString(), false);
	 	   Log.d("buttons", "button Send was pressed");
	 	
	 	   Thread t = new Thread(new Runnable(){
				
		    	public void run(){
		    		postResult = 0;
		    		postResult = newPost.makePost();
		    		return;
		    	}
		    });
	 	   t.start();
	  	    try {
				t.join();
			
	  	    switch(postResult){
			case 0:
				Toast.makeText(this, "Message #" + message_id + " posted", Toast.LENGTH_LONG).show();
				showDialog(1);
				
				
				break;
			case 1: 
				
				Toast.makeText(contex, "Something wrong with server response.", Toast.LENGTH_LONG).show();
			
			break;
			case 2:
				Toast.makeText(contex, "Something wrong with connection", Toast.LENGTH_LONG).show();
				break;
			case 3: 
				Toast.makeText(contex, "IO error, try reloading the application", Toast.LENGTH_LONG).show();
				break;
			}
	  	 } catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	  	    	
	}
	public void onClickCancel(View v){
		finish();
	}
	public void getIntentExtra(){
		Intent intent = getIntent();
		if(intent.hasExtra("link")){
			etPostText.setText("\n\n" + intent.getStringExtra("link") + ".jpg" + "\n\n");
			return;
		}
		String action = intent.getAction();
		String type = intent.getType();
		String action_required = Intent.ACTION_SEND;
		String type_required = "text/plain";
		
		if(Intent.ACTION_SEND.equals(action) && type != null) {
			if(type_required.equals(type)) {
				String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
				etPostText.setText(sharedText);
				
			}
		}
	}
	
}
