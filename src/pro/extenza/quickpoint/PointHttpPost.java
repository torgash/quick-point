package pro.extenza.quickpoint;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.http.AndroidHttpClient;
import android.util.Log;


public class PointHttpPost {
	SharedPreferences prefs;
	String URLHost = "http://point.im/api/";
	String login;
	String password;
	String URLapi;
	String result="";
	String postText;
	String postTags;
	String _private;
	String csrf_token;
	String token;
	public PointHttpPost(Context contex, String _URL, String _login, String _password) {
		// TODO Auto-generated constructor stub
		//Авторизация. Отправляем логин и пароль, получаем два токена, сохраняем в SharedPref.
		prefs = contex.getSharedPreferences("prefs", 0);
		login = _login;
		password = _password;
		URLapi = URLHost + _URL;
	}
	public PointHttpPost(String URL) {
		// TODO Auto-generated constructor stub
		//токен брать из SharedPref
	}
	public PointHttpPost(Context contex, String URL, String text, String tag, boolean privat) {
		// TODO Auto-generated constructor stub
		//создание поста. Токен посылать формата X-CSRF: <csrf_token>
		prefs = contex.getSharedPreferences("prefs", 0);
		token = prefs.getString("token", "");
		csrf_token = prefs.getString("csrf_token", "");
		postText = text;
		postTags = tag;
		_private = String.valueOf(privat);
	}
	int postResult;
	public int makePost(){
	 	HttpClient httpclient = new DefaultHttpClient();
	 	
	    
	    HttpPost httppost = new HttpPost("http://point.im/api/post");
	    
	    try {
	        // Add your data
	        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);
	        nameValuePairs.add(new BasicNameValuePair("Content-Type", "text/html; charset=utf-8"));
	        String[] postTagsSplit = postTags.split(",");
	        nameValuePairs.add(new BasicNameValuePair("text", postText));
	        for(int i=0; i<postTagsSplit.length; i++){
	        nameValuePairs.add(new BasicNameValuePair("tag", postTagsSplit[i].trim()));
	        }
	        //nameValuePairs.add(new BasicNameValuePair("private", "1"));
	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, HTTP.UTF_8));
	        httppost.setHeader("Authorization", token);
	        httppost.setHeader("X-CSRF", csrf_token);
	        Log.d("DOWNLOAD", httppost.getFirstHeader("X-CSRF").toString());
	        Log.d("DOWNLOAD", httppost.getParams().toString());
	        // Execute HTTP Post Request
	        HttpResponse response = httpclient.execute(httppost);
	        HttpEntity httpEntity=response.getEntity();
	        InputStream stream = AndroidHttpClient.getUngzippedContent(httpEntity);
		    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
            StringBuilder responseBuilder= new StringBuilder();
            char[] buff = new char[1024*512];
            int read;
            while((read = bufferedReader.read(buff)) != -1) {
                responseBuilder.append(buff, 0, read) ;
                Log.d("DOWNLOAD", "Downloaded " + responseBuilder.length());
                Log.d("DOWNLOAD", responseBuilder.toString());
            }
            result = responseBuilder.toString();
           
            try {
                JSONObject obj = new JSONObject(new JSONTokener(result));
                	if(obj.has("error")) return 1;
                      String ok = obj.getString("id");
                      
                      Log.d("JSON","id = " + ok);
                    MainActivity.message_id = ok;
                      if(ok != null){
                    	  return 0;
                      }
                 
                
              } catch (JSONException jsonEx) {
                Log.e("JSON","Error parsing data "+jsonEx.toString());
                return 1;
                //Toast.makeText(getApplicationContext(), "fail", Toast.LENGTH_SHORT).show();
              }
            
            
	    
	    
	    } catch (ClientProtocolException e) {
	    	return 2;
	        // TODO Auto-generated catch block
	    } catch (IOException e) {
	    	return 3;
	        // TODO Auto-generated catch block
	    }
	    return 0;
		
		
		
	}
	public int getTokens(){
	 	
	 	HttpClient httpclient = new DefaultHttpClient();
	 	
			    
			    HttpPost httppost = new HttpPost("http://point.im/api/login/");
			    
			    try {
			        // Add your data
			        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			        nameValuePairs.add(new BasicNameValuePair("login", login));
			        nameValuePairs.add(new BasicNameValuePair("password", password));
			        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			        // Execute HTTP Post Request
			        HttpResponse response = httpclient.execute(httppost);
			        HttpEntity httpEntity=response.getEntity();
			        InputStream stream = AndroidHttpClient.getUngzippedContent(httpEntity);
				    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
		            StringBuilder responseBuilder= new StringBuilder();
		            char[] buff = new char[1024*512];
		            int read;
		            while((read = bufferedReader.read(buff)) != -1) {
		                responseBuilder.append(buff, 0, read) ;
		                Log.d("DOWNLOAD", "скачано " + responseBuilder.length());
		            }
		            result = responseBuilder.toString();
		           
		            try {
		                JSONObject obj = new JSONObject(new JSONTokener(result));
		                	if(obj.has("error")) return 1;
		                      String token = obj.getString("token");
		                      String csrf_token = obj.getString("csrf_token");
		                      Log.d("JSON","token = " + token);
		                      Log.d("JSON", "csrf_token = " + csrf_token);
		                      if(token != null){
		                    	  SharedPreferences.Editor editor = prefs.edit();
		                    	  editor.putString("token", token);
		                    	  editor.putString("csrf_token", csrf_token);
		                    	  editor.apply();
		                    	  Log.d("PREFS", "token put to prefs is: " + prefs.getString("token", "nothing"));
		                    	  Log.d("PREFS", "csrf_token put to prefs is: " + prefs.getString("csrf_token", "nothing"));
		                      }
		                 
		                
		              } catch (JSONException jsonEx) {
		                Log.e("JSON","Error parsing data "+jsonEx.toString());
		                return 1;
		                //Toast.makeText(getApplicationContext(), "fail", Toast.LENGTH_SHORT).show();
		              }
		            
		            
			    
			    
			    } catch (ClientProtocolException e) {
			    	return 2;
			        // TODO Auto-generated catch block
			    } catch (IOException e) {
			    	return 3;
			        // TODO Auto-generated catch block
			    }
			    return 0;
	 	
	}
	public static SharedPreferences getSharedPreferences (Context ctxt) {
		   return ctxt.getSharedPreferences("FILE", 0);
		}
}
