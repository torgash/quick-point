package pro.extenza.quickpoint;
//Fragment of pop-up window with Yandex first-time login.
//Must be set up in parent activity to pop-up only 
//when token isn't still aquired or has expired.
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import android.app.DialogFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

public class YaServiceProgressDialog extends DialogFragment {
TextView tvProcessName;
ProgressBar pbProgress;
	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		super.onDestroyView();
	}

	final String LOG_TAG = "QUICKPOINT";

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		getDialog().setTitle("Ya.Fotki");
		View v = inflater.inflate(R.layout.ya_progress_dialog, null);
		tvProcessName = (TextView)v.findViewById(R.id.tvProcessName);
		pbProgress = (ProgressBar)v.findViewById(R.id.pbYaServiceProgress);
		
		
		return v;
	}
	
	public void onProcessChanged(String process){
		tvProcessName.setText(process);
	}
}