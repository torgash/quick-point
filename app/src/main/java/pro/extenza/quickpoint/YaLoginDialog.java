package pro.extenza.quickpoint;
//Fragment of pop-up window with Yandex first-time login.
//Must be set up in parent activity to pop-up only 
//when token isn't still aquired or has expired.
import android.app.DialogFragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class YaLoginDialog extends DialogFragment {

	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		super.onDestroyView();
	}

	final String LOG_TAG = "myLogs";

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		getDialog().setTitle("Yandex Login");
		View v = inflater.inflate(R.layout.yalogindig, null);
		WebView yaLoginWebView = (WebView) v.findViewById(R.id.loginWebView);
		yaLoginWebView.setWebViewClient(new WebViewClient() {
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				// do your handling codes here, which url is the requested url
				// probably you need to open that url rather than redirect:
				Log.d("DOWNLOAD", "Redirecting to " + url);
				if (url.contains("quickpoint")) {
					YaPhotoActivity callingActivity = (YaPhotoActivity) getActivity();
			        callingActivity.getTokenUrl(url);
			        dismiss();

					Log.d("DOWNLOAD",
							"trying to load and see if the intent is called");
					return true;

				}
				view.loadUrl(url);
				return false; // then it is not handled by default action
			}
		});
		String loginURL = "https://oauth.yandex.ru/authorize?response_type=token&client_id="
				+ getString(R.string.client_id) + "&display=popup";
		yaLoginWebView.loadUrl(loginURL);
		Log.d("DOWNLOAD", "loading string " + loginURL);
		return v;
	}

}