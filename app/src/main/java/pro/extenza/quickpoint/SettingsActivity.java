package pro.extenza.quickpoint;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.util.Log;

public class SettingsActivity extends PreferenceActivity {
	SharedPreferences prefs;
	Context contex;
final String TAG = "QUICKPOINT";
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		contex = getApplicationContext();
		prefs = contex.getSharedPreferences("prefs", MODE_PRIVATE);
		addPreferencesFromResource(R.xml.pref);

		Preference myPref = (Preference) findPreference("pointRelogin");
        Preference useDefTags = (Preference)findPreference("useDefTags");
        final Preference defTags = (Preference)findPreference("defTags");
        Preference resolution = (Preference)findPreference("resolution");
        Preference yaUseDefTags = (Preference)findPreference("yaUseDefTags");
        final Preference yaDefTags = (Preference)findPreference("yaDefTags");
        defTags.setEnabled(useDefTags.getExtras().getBoolean("useDefTags"));
        yaDefTags.setEnabled(yaUseDefTags.getExtras().getBoolean("yaUseDefTags"));
		myPref.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			public boolean onPreferenceClick(Preference preference) {
				// open browser or intent here
				Editor editor = prefs.edit();
				editor.remove("csrf_token");
				editor.apply();
				Intent intent = new Intent(contex, MainActivity.class);
				startActivity(intent);
				return true;
			}
		});

		useDefTags.setOnPreferenceChangeListener(new OnPreferenceChangeListener(){

			@Override
			public boolean onPreferenceChange(Preference preference,
					Object newValue) {
				// TODO Auto-generated method stub
				Editor editor = prefs.edit();
				editor.putBoolean("useDefTags", (Boolean)newValue);
				editor.apply();
                defTags.setEnabled(((Boolean) newValue).booleanValue());
				return true;
			}
			
		});

		defTags.setOnPreferenceChangeListener(new OnPreferenceChangeListener(){

			@Override
			public boolean onPreferenceChange(Preference preference,
					Object newValue) {
				// TODO Auto-generated method stub
				Editor editor = prefs.edit();
				editor.putString("defTags", (String)newValue);
				editor.apply();
				return true;
			}
			
		});

		resolution.setOnPreferenceChangeListener(new OnPreferenceChangeListener(){

			@Override
			public boolean onPreferenceChange(Preference preference,
					Object newValue) {
				// TODO Auto-generated method stub
				Log.d(TAG, "preference resolution newValue: " + newValue.getClass().toString() + ", " + newValue.toString()	);
				Editor editor = prefs.edit();
				editor.putInt("resolution", new Integer((String)newValue));
				editor.apply();
				return true;
			}
			
		});

		yaUseDefTags.setOnPreferenceChangeListener(new OnPreferenceChangeListener(){

			@Override
			public boolean onPreferenceChange(Preference preference,
					Object newValue) {
				Log.d(TAG, "preference resolution newValue: " + newValue.getClass().toString() + ", " + newValue.toString()	);
				// TODO Auto-generated method stub
				Editor editor = prefs.edit();
				editor.putBoolean("yaUseDefTags", (Boolean)newValue);
				editor.apply();
                yaDefTags.setEnabled(((Boolean) newValue).booleanValue());
				return true;
			}
			
		});

		yaDefTags.setOnPreferenceChangeListener(new OnPreferenceChangeListener(){

			@Override
			public boolean onPreferenceChange(Preference preference,
					Object newValue) {
				Log.d(TAG, "preference resolution newValue: " + newValue.getClass().toString() + ", " + newValue.toString()	);
				// TODO Auto-generated method stub
				Editor editor = prefs.edit();
				editor.putString("yaDefTags", (String)newValue);
				editor.apply();

				return true;
			}
			
		});

	}

}