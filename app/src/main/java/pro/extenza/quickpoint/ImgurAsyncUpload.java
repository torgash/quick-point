package pro.extenza.quickpoint;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * Created by ivanblch on 04.01.2015.
 */
public class ImgurAsyncUpload {

    Uri uri;
    String url;
    String client_id
    File image;
    private static AsyncHttpClient client = new AsyncHttpClient();
    public ImgurAsyncUpload(String _client_id, String _url, Uri _uri) {

        client_id = _client_id;
        uri = _uri;
        url = _url;
        image = new File(uri.getPath());

    }
    public String imageUpload(){
        if(!image.exists()) return null;
        RequestParams params = new RequestParams();
        try {
            params.put("image", image);
        } catch(FileNotFoundException e) {}
        client.addHeader("Authorization",
                "Client-ID " + client_id);
        client.post()
    }
}
