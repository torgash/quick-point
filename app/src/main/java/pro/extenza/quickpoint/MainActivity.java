package pro.extenza.quickpoint;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {
	public static final String USER_LOGIN = "USER_LOGIN";
	public static final String USER_PASSWORD = "USER_PASSWORD";
	public static final String USER_TOKEN = "USER_TOKEN";
	public static final String USER_CSRF = "USER_CSRF";
	SharedPreferences prefs;
	Context contex;
	Button btnSend;
	Button btnCancel;
	EditText etPostText;
	EditText etPostTags;
	Button btnLogin;
	EditText etLogin;
	EditText etPassword;
	public static String message_id;
	int authResult;
	String defTags;
	Boolean useDefTags;
	String yaDefTags;
	Boolean yaUseDefTags;
	ProgressDialog progressDiag;
	final String TAG = "QUICKPOINT";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		contex = getApplicationContext();
		prefs = contex.getSharedPreferences("prefs", MODE_PRIVATE);
		Log.d("DOWNLOAD", prefs.getString("csrf_token", ""));
		if (prefs.contains("csrf_token")) {
			setContentView(R.layout.main);
			btnSend = (Button) findViewById(R.id.btnSend);
			btnCancel = (Button) findViewById(R.id.btnCancel);
			etPostText = (EditText) findViewById(R.id.etPostText);
			etPostTags = (EditText) findViewById(R.id.etPostTags);

            useDefTags = prefs.getBoolean("useDefTags", false);
            if(useDefTags) defTags = prefs.getString("defTags", "");
            yaUseDefTags = prefs.getBoolean("yaUseDefTags", false);
            if(yaUseDefTags) yaDefTags = prefs.getString("yaDefTags", "");

            if (useDefTags  && (etPostTags.getText().equals("") || !etPostTags
                    .getText().equals(yaDefTags)))
                etPostTags.setText(defTags);

			getIntentExtra();

		} else {
			setContentView(R.layout.login);
			btnLogin = (Button) findViewById(R.id.btnLogin);
			etLogin = (EditText) findViewById(R.id.etLogin);
			etPassword = (EditText) findViewById(R.id.etPassword);
		}

	}

	protected void onResume() {

        useDefTags = prefs.getBoolean("useDefTags", false);
        if(useDefTags) defTags = prefs.getString("defTags", "");
        yaUseDefTags = prefs.getBoolean("yaUseDefTags", false);
        if(yaUseDefTags) yaDefTags = prefs.getString("yaDefTags", "");

        if (useDefTags  && (etPostTags.getText().equals("") || !etPostTags
                .getText().equals(yaDefTags)))
            etPostTags.setText(defTags);
		super.onResume();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuItem mi = menu.add(0, 1, 0, getString(R.string.preferences));
		mi.setIntent(new Intent(this, SettingsActivity.class));
		return super.onCreateOptionsMenu(menu);
	}

	protected Dialog onCreateDialog(int id) {

		OnClickListener myClickListener = new OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {

				case Dialog.BUTTON_POSITIVE:
					MainActivity.this.finish();

					break;
				case Dialog.BUTTON_NEGATIVE:
					Intent webIntent = new Intent();
					webIntent.setAction(Intent.ACTION_VIEW);
					String messageURL = "http://point.im/" + message_id;

					webIntent.setData(Uri.parse(messageURL));
					startActivity(webIntent);
					finish();
				}
			}
		};
		AlertDialog.Builder adb = new AlertDialog.Builder(this);

		adb.setTitle("POSTED");

		switch (id) {
		case 1:
			adb.setMessage("Your message: #" + message_id);
			break;

		}


		adb.setIcon(android.R.drawable.ic_dialog_alert);

		adb.setPositiveButton("OK", myClickListener);
		adb.setNegativeButton("WEB", myClickListener);
		return adb.create();

	}

    public void registerIntent(View view) {
        Intent webIntent = new Intent();
        webIntent.setAction(Intent.ACTION_VIEW);
        String messageURL = "http://point.im/register";

        webIntent.setData(Uri.parse(messageURL));
        startActivity(webIntent);
    }

    public class LoginTask extends AsyncTask<Void, Void, Integer> {
		public void onPreExecute() {
			

			progressDiag = ProgressDialog.show(MainActivity.this, "Please wait",
					"Logging in...", true, false);
		}

		@Override
		protected Integer doInBackground(Void... params) {
			// TODO Auto-generated method stub
			final PointHttpPost authorize = new PointHttpPost(contex, "/login",
					etLogin.getText().toString(), etPassword.getText().toString());
			Log.d("buttons", "button was pressed");
			
			authResult = 3;
			authResult = authorize.getTokens();
			Log.d(TAG, "tokens are finally here");
			return authResult;
		}
		@Override
		public void onPostExecute(final Integer result) {
			
			Log.d(TAG, "We've come to postExecute by login");
			
			progressDiag.dismiss();
			switch (result) {
			case 0:
				setContentView(R.layout.main);
				btnSend = (Button) findViewById(R.id.btnSend);
				btnCancel = (Button) findViewById(R.id.btnCancel);
				etPostText = (EditText) findViewById(R.id.etPostText);
				etPostTags = (EditText) findViewById(R.id.etPostTags);
				if (useDefTags = true && (etPostTags.getText().equals("") || !etPostTags
						.getText().equals(yaDefTags)))
					etPostTags.setText(defTags);
				
				getIntentExtra();
				return;
			case 1:
				etLogin.setText("");
				etPassword.setText("");
				etLogin.requestFocus();
				
				Toast.makeText(contex, "Not authorized.\nWrong password?",
						Toast.LENGTH_LONG).show();

				break;
			case 2:
				Toast.makeText(contex, "Something wrong with connection",
						Toast.LENGTH_LONG).show();
				
				break;
			case 3:
				Toast.makeText(contex,
						"IO error, try reloading the application",
						Toast.LENGTH_LONG).show();
				
				break;
			}
		return;

		}

	}
	public void onClickLogin(View v) {
		new LoginTask().execute();
	}

	int postResult;

	public class SendPostTask extends AsyncTask<Void, Void, Integer> {
        ProgressDialog progressD;
        public void onPreExecute() {

            progressD = new ProgressDialog(MainActivity.this);
            progressD.setTitle("Please wait...");
            progressD.setCancelable(false);
            progressD.setIndeterminate(true);
            progressD.setMessage("Posting in progress...");
            progressD.show();

		}

		@Override
		protected Integer doInBackground(Void... params) {
			// TODO Auto-generated method stub
            PointHttpPost newPost;

                newPost = new PointHttpPost(contex, "/post",
                        etPostText.getText().toString(), etPostTags.getText()
                        .toString(), false);

			Log.d("buttons", "button Send was pressed");
			postResult = 0;
			postResult = newPost.makePost();
			Log.d(TAG, "Got a postResult from posting");

            return 0;
		}

        public void onCancelled(){
            progressD.dismiss();
            super.onCancelled();
        }
		public void onPostExecute(Integer result) {


			progressD.dismiss();
            Log.d(TAG, "We've come to PostExecute");
			switch (postResult) {
			case 0:
				Toast.makeText(MainActivity.this,
						"Message #" + message_id + " posted", Toast.LENGTH_LONG)
						.show();
				showDialog(1);

				break;
			case 1:

				Toast.makeText(contex, "Something wrong with server response.",
						Toast.LENGTH_LONG).show();

				break;
			case 2:
				Toast.makeText(contex, "Something wrong with connection",
						Toast.LENGTH_LONG).show();
				break;
			case 3:
				Toast.makeText(contex,
						"IO error, try reloading the application",
						Toast.LENGTH_LONG).show();
				break;
			}
            return;
		}

	}

	public void onClickSend(View v) {
		new SendPostTask().execute();

	}

	public void onClickCancel(View v) {
		finish();
	}

	public void getIntentExtra() {
		Intent intent = getIntent();
		if (intent.hasExtra("link")) {
			etPostText.setText(intent.getStringExtra("link")+ "\n\n");
			if (yaUseDefTags) {
				etPostTags.setText(yaDefTags);
			}
			return;
		}
		String action = intent.getAction();
		String type = intent.getType();
		
		String type_required = "text/plain";

		if (Intent.ACTION_SEND.equals(action) && type != null) {
			if (type_required.equals(type)) {
				String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);

				etPostText.setText(sharedText);

			}
		}
	}
public void onStop(){
    ProgressDialog progressD = new ProgressDialog(MainActivity.this);

    progressD.show();
    if(progressD.isShowing()) progressD.dismiss();
    super.onStop();
}

}
